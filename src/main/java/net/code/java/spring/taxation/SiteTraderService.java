/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.code.java.spring.taxation;

import java.util.List;

/**
 *
 * @author Rok Koren
 */
public interface SiteTraderService {
    
    List<SiteTrader> findAll();
    SiteTrader save(SiteTrader siteTrader);
    SiteTrader findById(long traderId);   
}
