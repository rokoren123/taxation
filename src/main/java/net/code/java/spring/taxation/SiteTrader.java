/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.code.java.spring.taxation;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Rok Koren
 */
@Entity
public class SiteTrader {
    
    public static final int TAXATION_TYPE_GENERAL  = 1;
    public static final int TAXATION_TYPE_WINNINGS = 2;
    
    public static final int TAXATION_VALUE_TYPE_RATE   = 1;    
    public static final int TAXATION_VALUE_TYPE_AMOUNT = 2;    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    private long traderId;
    
    private String country;
    private double taxationValue;
    private int taxationType;
    private int taxationValueType;

    public SiteTrader(String country, double taxationValue, int taxationType, int taxationValueType) {
        this.country = country;
        this.taxationValue = taxationValue;
        this.taxationType = taxationType;
        this.taxationValueType = taxationValueType;
    }    
    
    public long getTraderId() {
        return traderId;
    }
    
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }    

    public double getTaxationValue() {
        return taxationValue;
    }

    public void setTaxationValue(double taxationValue) {
        this.taxationValue = taxationValue;
    }

    public int getTaxationType() {
        return taxationType;
    }

    public void setTaxationType(int taxationType) {
        this.taxationType = taxationType;
    }
    
    public int getTaxationValueType() {
        return taxationValueType;
    }

    public void setTaxationValueType(int taxationValueType) {
        this.taxationValueType = taxationValueType;
    }    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SiteTrader siteTrader = (SiteTrader) o;
        return Objects.equals(traderId, siteTrader.traderId);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(traderId);
    }
 
    @Override
    public String toString() {
        return String.format("Site Trader [traderId=%s, country=%s, taxationValue=%s, taxationType=%s, taxationValueType=%s]", this.traderId, this.country, this.taxationValue, this.taxationType, this.taxationValueType);
    }  
}
