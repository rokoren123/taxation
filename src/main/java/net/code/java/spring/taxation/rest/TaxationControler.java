/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.code.java.spring.taxation.rest;

import net.code.java.spring.taxation.SiteTrader;
import net.code.java.spring.taxation.SiteTraderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Rok Koren
 */
@RestController
@RequestMapping("/taxation")
public class TaxationControler {
    
    private final SiteTraderService siteTraderService;

    public TaxationControler(SiteTraderService siteTraderService) {
        this.siteTraderService = siteTraderService;
    }
 
    @GetMapping
    public Outgoing taxation(@RequestParam(value = "traderId") long traderId, @RequestParam(value = "playedAmount") double playedAmount, @RequestParam(value = "odd") double odd) {
        SiteTrader siteTrader = siteTraderService.findById(traderId);
        if(siteTrader != null)
        {
            double amount = playedAmount * odd;
            if(siteTrader.getTaxationType() == SiteTrader.TAXATION_TYPE_GENERAL)
            {
                if(siteTrader.getTaxationValueType() == SiteTrader.TAXATION_VALUE_TYPE_RATE)
                {
                    double taxation = amount * (siteTrader.getTaxationValue() / 100D);
                    double possibleReturnAmount = amount - taxation;                
                    return new Outgoing(possibleReturnAmount, amount, possibleReturnAmount, siteTrader.getTaxationValue(), null);                    
                }
                else if(siteTrader.getTaxationValueType() == SiteTrader.TAXATION_VALUE_TYPE_AMOUNT)
                {
                    double possibleReturnAmount = amount - siteTrader.getTaxationValue();                
                    return new Outgoing(possibleReturnAmount, amount, possibleReturnAmount, siteTrader.getTaxationValue(), null);                    
                }
            }
            else if(siteTrader.getTaxationType() == SiteTrader.TAXATION_TYPE_WINNINGS)
            {
                if(siteTrader.getTaxationValueType() == SiteTrader.TAXATION_VALUE_TYPE_RATE)
                {
                    double winingsAmount = amount - playedAmount;     
                    double taxation = winingsAmount * (siteTrader.getTaxationValue() / 100D);
                    double possibleReturnAmount = amount - taxation;
                    return new Outgoing(possibleReturnAmount, amount, possibleReturnAmount, siteTrader.getTaxationValue(), null);   
                }
                else if(siteTrader.getTaxationValueType() == SiteTrader.TAXATION_VALUE_TYPE_AMOUNT)
                {
                    double winingsAmount = amount - playedAmount;     
                    double taxation = winingsAmount - siteTrader.getTaxationValue();
                    double possibleReturnAmount = amount - taxation;
                    return new Outgoing(possibleReturnAmount, amount, possibleReturnAmount, siteTrader.getTaxationValue(), null);  
                }                                                           
            }
        }
        return null;
    }
    
    @PostMapping
    public ResponseEntity<Outgoing> taxation(@RequestBody Incoming incoming) {
        SiteTrader siteTrader = siteTraderService.findById(incoming.traderId);
        if(siteTrader != null)
        {
            double amount = incoming.getPlayedAmount() * incoming.getOdd();
            if(siteTrader.getTaxationType() == SiteTrader.TAXATION_TYPE_GENERAL)
            {
                if(siteTrader.getTaxationValueType() == SiteTrader.TAXATION_VALUE_TYPE_RATE)
                {
                    double taxation = amount * (siteTrader.getTaxationValue() / 100D);
                    double possibleReturnAmount = amount - taxation;                
                    Outgoing outgoing = new Outgoing(possibleReturnAmount, amount, possibleReturnAmount, siteTrader.getTaxationValue(), null);   
                    ResponseEntity.ok().body(outgoing);
                }
                else if(siteTrader.getTaxationValueType() == SiteTrader.TAXATION_VALUE_TYPE_AMOUNT)
                {
                    double possibleReturnAmount = amount - siteTrader.getTaxationValue();                
                    Outgoing outgoing = new Outgoing(possibleReturnAmount, amount, possibleReturnAmount, siteTrader.getTaxationValue(), null);    
                    ResponseEntity.ok().body(outgoing);
                }
            }
            else if(siteTrader.getTaxationType() == SiteTrader.TAXATION_TYPE_WINNINGS)
            {
                if(siteTrader.getTaxationValueType() == SiteTrader.TAXATION_VALUE_TYPE_RATE)
                {
                    double winingsAmount = amount - incoming.getPlayedAmount();     
                    double taxation = winingsAmount * (siteTrader.getTaxationValue() / 100D);
                    double possibleReturnAmount = amount - taxation;
                    Outgoing outgoing = new Outgoing(possibleReturnAmount, amount, possibleReturnAmount, siteTrader.getTaxationValue(), null);   
                    ResponseEntity.ok().body(outgoing);
                }
                else if(siteTrader.getTaxationValueType() == SiteTrader.TAXATION_VALUE_TYPE_AMOUNT)
                {
                    double winingsAmount = amount - incoming.getPlayedAmount();     
                    double taxation = winingsAmount - siteTrader.getTaxationValue();
                    double possibleReturnAmount = amount - taxation;
                    Outgoing outgoing = new Outgoing(possibleReturnAmount, amount, possibleReturnAmount, siteTrader.getTaxationValue(), null);  
                    ResponseEntity.ok().body(outgoing);
                }                                                           
            }
        }
        return ResponseEntity.notFound().build();
    }     

    public static class Incoming {

        private long traderId;
        private double playedAmount;
        private double odd;

        public Incoming(long traderId, double playedAmount, double odd) {
            this.traderId = traderId;
            this.playedAmount = playedAmount;
            this.odd = odd;
        }

        public long getTraderId() {
            return traderId;
        }

        public void setTraderId(long traderId) {
            this.traderId = traderId;
        }

        public double getPlayedAmount() {
            return playedAmount;
        }

        public void setPlayedAmount(double playedAmount) {
            this.playedAmount = playedAmount;
        }

        public double getOdd() {
            return odd;
        }

        public void setOdd(double odd) {
            this.odd = odd;
        }    
    }    
    
    public static class Outgoing {

        private double possibleReturnAmount;
        private double possibleReturnAmountBefTax;
        private double possibleReturnAmountAfterTax;
        private Double taxRate;
        private Double taxAmount;

        public Outgoing(double possibleReturnAmount, double possibleReturnAmountBefTax, double possibleReturnAmountAfterTax, Double taxRate, Double taxAmount) {
            this.possibleReturnAmount = possibleReturnAmount;
            this.possibleReturnAmountBefTax = possibleReturnAmountBefTax;
            this.possibleReturnAmountAfterTax = possibleReturnAmountAfterTax;
            this.taxRate = taxRate;
            this.taxAmount = taxAmount;
        }

        public double getPossibleReturnAmount() {
            return possibleReturnAmount;
        }

        public void setPossibleReturnAmount(double possibleReturnAmount) {
            this.possibleReturnAmount = possibleReturnAmount;
        }

        public double getPossibleReturnAmountBefTax() {
            return possibleReturnAmountBefTax;
        }

        public void setPossibleReturnAmountBefTax(double possibleReturnAmountBefTax) {
            this.possibleReturnAmountBefTax = possibleReturnAmountBefTax;
        }

        public double getPossibleReturnAmountAfterTax() {
            return possibleReturnAmountAfterTax;
        }

        public void setPossibleReturnAmountAfterTax(double possibleReturnAmountAfterTax) {
            this.possibleReturnAmountAfterTax = possibleReturnAmountAfterTax;
        }

        public Double getTaxRate() {
            return taxRate;
        }

        public void setTaxRate(Double taxRate) {
            this.taxRate = taxRate;
        }

        public Double getTaxAmount() {
            return taxAmount;
        }

        public void setTaxAmount(Double taxAmount) {
            this.taxAmount = taxAmount;
        }        
    }    
}
