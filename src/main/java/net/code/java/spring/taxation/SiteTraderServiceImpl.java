/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.code.java.spring.taxation;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rok Koren
 */
@Service
public class SiteTraderServiceImpl implements SiteTraderService {
 
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public List<SiteTrader> findAll() {
        Query query = em.createQuery("SELECT c FROM SiteTrader c", SiteTrader.class);
        return query.getResultList();
    }
    
    @Override
    public SiteTrader save(SiteTrader siteTrader) {
        em.persist(siteTrader);
        return siteTrader;
    }
    
    @Override
    public SiteTrader findById(long traderId) {
        return em.find(SiteTrader.class, traderId);
    }  
}
