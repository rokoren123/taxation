package net.code.java.spring.taxation;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TaxationApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaxationApplication.class, args);
    }

    /*
    @Bean
    public ApplicationRunner siteTradersInitializer(SiteTraderService siteTraderService) {
        return args -> {
            if(siteTraderService.findAll().isEmpty())
            {
                siteTraderService.save(new SiteTrader("Slovenia", 10D, SiteTrader.TAXATION_TYPE_GENERAL, SiteTrader.TAXATION_VALUE_TYPE_RATE));
                siteTraderService.save(new SiteTrader("Austria", 2D, SiteTrader.TAXATION_TYPE_GENERAL, SiteTrader.TAXATION_VALUE_TYPE_AMOUNT));
                siteTraderService.save(new SiteTrader("Germany", 8D, SiteTrader.TAXATION_TYPE_WINNINGS, SiteTrader.TAXATION_VALUE_TYPE_RATE));                
            }
        };
    } 
    */
}
